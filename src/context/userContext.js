import { createContext } from "react";
export default createContext({
    session: {
        userName: null,
        isConnected: false,
    },
    setSession: () => { },
});