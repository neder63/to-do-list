import { createContext } from "react";

export default createContext({
    taskList: null,
    setTaskList: () => { },
});
