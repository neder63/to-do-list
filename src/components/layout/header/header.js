import React from "react";
import logoutIcon from "../../../aseets/logout.png"
import "./header.css"

const Header = ({ isAuthenticated, setSession }) => {
    const Logout = (e) => {
        e.preventDefault()
        setSession({
            userName: null,
            isConnected: false
        })
    }
    return (
        <div className="header">
            <span className="title">Todo List</span>
            {isAuthenticated && <div className="btn-logout" onClick={Logout}>
                <img src={logoutIcon} alt="logout" />

            </div>}
        </div>
    )
}

export default Header;