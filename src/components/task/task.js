import React, { Fragment } from "react";
import "./task.css";


const Task = ({
    item,
    taskCompleted,
    deletetask,
    modifyTask,
}) => {
    return (
        <Fragment>
            <div className={item.isCompleted ? "crossText" : "listitem"}>
                {`${item.name}: ${item.description}`}
                <button
                    className="completed"
                    onClick={(e) => taskCompleted(e, item.id)}
                    style={{ backgroundColor: item.isCompleted ? "green" : "red" }}
                >
                    {item.isCompleted ? "complétée" : "non complétée"}
                </button>
                <button className="delete" onClick={(e) => deletetask(e, item.id)}>
                    supprimer
              </button>
                <button className="modify" onClick={(e) => modifyTask(e, item)}>
                    Modifier
              </button>
            </div>
        </Fragment>
    )
}

export default Task;