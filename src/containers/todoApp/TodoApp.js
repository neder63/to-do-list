import React, { useState } from "react";
import tasksContext from ".././../context/tasksContext";
import { useLocalStorage } from "../../hooks/useLocalStorage";
import Task from "../../components/task/task"
import "./todoapp.css";

const TodoApp = () => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("")
  const [tasklist, setTaskList] = useLocalStorage("tasklist", []);
  const [tasktoModify, setTasktoModify] = useState(false);
  const handleChangeName = (e) => {
    setName(e.target.value);
  };
  const handleChangeDescription = (e) => {
    setDescription(e.target.value);
  };
  const AddTask = () => {
    if (name !== "") {
      const taskDetails = {
        id: Math.floor(Math.random() * 1000),
        name: name,
        description: description,
        isCompleted: false,
      };

      setTaskList([...tasklist, taskDetails]);
      setName("");
      setDescription("");
    }
  };

  const deletetask = (e, id) => {
    e.preventDefault();
    setTaskList(tasklist.filter((t) => t.id !== id));
  };

  const taskCompleted = (e, id) => {
    e.preventDefault();
    //let's find index of element
    const element = tasklist.findIndex((elem) => elem.id === id);

    //copy array into new variable
    const newTaskList = [...tasklist];

    //edit our element
    newTaskList[element] = {
      ...newTaskList[element],
      isCompleted: !tasklist[element].isCompleted,
    };

    setTaskList(newTaskList);
  };
  const clearForm = () => {
    setTasktoModify(null);
    setName("");
    setDescription("");
  }
  const handleModifyAction = (e) => {
    e.preventDefault();
    //let's find index of element
    const element = tasklist.findIndex((elem) => elem.id === tasktoModify);

    //copy array into new variable
    const newTaskList = [...tasklist];

    //edit our element
    newTaskList[element] = {
      ...newTaskList[element],
      name: name,
      description: description,
    };

    setTaskList(newTaskList);
    clearForm();
  }
  const modifyTask = (e, task) => {
    const { name, description, id } = task;
    e.preventDefault();
    setTasktoModify(id);
    setName(name);
    setDescription(description);
  }

  return (
    <tasksContext.Provider value={{ tasklist, setTaskList }}>
      <div className="todo">
        <div className="formWrapper">
          <input
            type="text"
            name="nom"
            onChange={(e) => handleChangeName(e)}
            value={name}
            placeholder="Nom de la tâche..."
          />
          <input
            type="text"
            name="description"
            value={description}
            onChange={(e) => handleChangeDescription(e)}
            placeholder="Description de la tâche..."
          />
          <button className="add-btn" onClick={tasktoModify ? handleModifyAction : AddTask}>
            {tasktoModify ? "modifier" : "Ajouter"}
          </button>
        </div>

        <br />
        {tasklist !== [] ? (
          <div className="taskWrapper">
            {tasklist.map((item, index) => (
              < Task
                item={item}
                taskCompleted={taskCompleted}
                deletetask={deletetask}
                modifyTask={modifyTask}
              />
            ))}
          </div>
        ) : null}
      </div>
    </tasksContext.Provider>
  );
}

export default TodoApp;
