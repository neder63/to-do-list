import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import UserContext from ".././../context/userContext";
import credentials from "../../constants/credentials";
import "./login.css";

const LoginPage = () => {
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState(null);
    const [password, setPassword] = useState("");
    const [passwordError, setPasswordError] = useState(null);
    const { session, setSession } = useContext(UserContext);
    const history = useHistory();

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    }

    const validateLoginForm = () => {
        setEmailError(email !== credentials.email);
        setPasswordError(password !== credentials.password);
    }

    const handleLoginAction = (e) => {
        validateLoginForm();
    }

    useEffect(() => {
        if (session.isConnected) {
            history.push("/todo")
        }
    }, [session.isConnected, history]);

    useEffect(() => {
        if (!!email && !!password) {
            if (!(emailError || passwordError)) {
                console.log("login sucess");
                setSession({
                    userName: email,
                    isConnected: true
                })
            }
        }
    }, [emailError, passwordError]);

    return (
        <div className="login">
            <div className="formWrapper">
                <input
                    type="text"
                    name="email"
                    id="text"
                    onChange={(e) => handleChangeEmail(e)}
                    value={email}
                    placeholder="Email"
                />
                <div className="errorWrapper">
                    {emailError && <span>Email invalid</span>}
                </div>
                <input
                    type="password"
                    name="password"
                    id="text"
                    value={password}
                    onChange={(e) => handleChangePassword(e)}
                    placeholder="Password"
                />
                <div className="errorWrapper">
                    {passwordError && <span>mot de passe invalid</span>}
                </div>
                <button className="add-btn" onClick={handleLoginAction}>
                    Login
                </button>
            </div>
        </div>
    )
}

export default LoginPage;
