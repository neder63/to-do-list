import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import UserContext from "./context/userContext";
import { useLocalStorage } from "./hooks/useLocalStorage";
import TodoContainer from "./containers/todoApp/TodoApp";
import LoginContainer from "./containers/login/login";
import ProtectedRoute from "./components/protectedRoute/ProtectedRoute";
import Header from "./components/layout/header/header"
import "./App.css";

function App() {
  const [session, setSession] = useLocalStorage("user", {
    userName: null,
    isConnected: null,
  });
  return (
    <UserContext.Provider value={{ session, setSession }}>
      <div className="App">
        <Header isAuthenticated={session.isConnected} setSession={setSession} />
        <BrowserRouter>
          <Switch>
            <Route path="/" component={LoginContainer} exact />
            <ProtectedRoute
              isAuthenticated={session.isConnected}
              path="/todo" Component={TodoContainer} />
          </Switch>
        </BrowserRouter>
      </div>
    </UserContext.Provider>
  );
}

export default App;
